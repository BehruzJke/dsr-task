import form from '../pages/form'
import testData from '../../fixtures/testData'

describe('template spec', () => {
  
  beforeEach('Open website',()=>{
    cy.visit('/')
  })

  it('Submit form contains all required elements', () => {
    form.elements.firstNameInput().should('be.visible')
    form.elements.lastNameInput().should('be.visible')
    form.elements.emailInput().should('be.visible')
    form.elements.phoneInput().should('be.visible')
    form.elements.genderInput("Male").should('be.visible')
    form.elements.genderInput("Female").should('be.visible')
    form.elements.agreementInput().should('be.visible')
    form.elements.submitBtn().should('be.visible')
  })
  it('User can\'t submit form with blank field',()=>{
    form.elements.submitBtn().click()
    form.elements.firstNameValidation().should('be.visible').should('have.css','color','rgb(224, 18, 18)')
    form.elements.lasttNameValidation().should('be.visible').should('have.css','color','rgb(224, 18, 18)')
    form.elements.emailValidation().should('be.visible').should('have.css','color','rgb(224, 18, 18)')
    form.elements.phoneValidation().should('be.visible').should('have.css','color','rgb(224, 18, 18)')
    form.elements.genderValidation().should('be.visible').should('have.css','color','rgb(224, 18, 18)')
    form.elements.agreementValidation().should('be.visible').should('have.css','color','rgb(224, 18, 18)')
  })
  it('User can\'t submit form with invalid firstname',()=>{
    form.elements.lastNameInput().type(testData.valid.lastName[0])
    form.elements.emailInput().type(testData.valid.email[0])
    form.elements.phoneInput().type(testData.valid.phone[0])
    form.elements.genderInput("Male").check()
    form.elements.agreementInput().check()
    for(let i=0;i < testData.invalid.firstName.length;i++){
      form.elements.firstNameInput().type(testData.invalid.firstName[i])
      form.elements.submitBtn().click()
      form.elements.firstNameValidation().should('be.visible')
      form.elements.firstNameInput().clear()
    }

  })
  it('User can\'t submit form with invalid lastname',()=>{
    form.elements.firstNameInput().type(testData.valid.firstName[0])
    form.elements.emailInput().type(testData.valid.email[0])
    form.elements.phoneInput().type(testData.valid.phone[0])
    form.elements.genderInput("Male").check()
    form.elements.agreementInput().check()
    for(let i=0;i < testData.invalid.lastName.length;i++){
      form.elements.lastNameInput().type(testData.invalid.lastName[i])
      form.elements.submitBtn().click()
      form.elements.lasttNameValidation().should('be.visible')
      form.elements.lastNameInput().clear()
    }
  })
  it('User can\'t submit form with invalid email',()=>{
    form.elements.firstNameInput().type(testData.valid.firstName[0])
    form.elements.lastNameInput().type(testData.valid.lastName[0])
    form.elements.emailInput().type(testData.invalid.email)
    form.elements.phoneInput().type(testData.valid.phone[0])
    form.elements.genderInput("Male").check()
    form.elements.agreementInput().check()
    form.elements.submitBtn().click()
    form.elements.emailValidation().should('be.visible')

  })
  it('User can\'t submit form with invalid phone number',()=>{
    form.elements.firstNameInput().type(testData.valid.firstName[0])
    form.elements.lastNameInput().type(testData.valid.lastName[0])
    form.elements.emailInput().type(testData.valid.email[0])
    form.elements.genderInput("Male").check()
    form.elements.agreementInput().check()
    for(let i=0;i < testData.invalid.phone.length;i++){
      form.elements.phoneInput().type(testData.invalid.phone[i])
      form.elements.submitBtn().click()
      form.elements.phoneValidation().should('be.visible')
      form.elements.phoneInput().clear()
    }
  })
  it('User can\'t submit form without selecting gender',()=>{
    form.elements.firstNameInput().type(testData.valid.firstName[0])
    form.elements.lastNameInput().type(testData.valid.lastName[0])
    form.elements.emailInput().type(testData.valid.email[0])
    form.elements.phoneInput().type(testData.valid.phone[0])
    form.elements.agreementInput().check()
    form.elements.submitBtn().click()
    form.elements.genderValidation().should('be.visible')
  })
  it('User can\'t submit form without agreeing to process personal data',()=>{
    form.elements.firstNameInput().type(testData.valid.firstName[0])
    form.elements.lastNameInput().type(testData.valid.lastName[0])
    form.elements.emailInput().type(testData.valid.email[0])
    form.elements.phoneInput().type(testData.valid.phone[0])
    form.elements.genderInput("Male").check()
    form.elements.submitBtn().click()
    form.elements.agreementValidation().should('be.visible')
  })
  it('User can submit form with all correct fields',()=>{
    for(let i=0; i <4; i++){
      for(let j=0; j <4; j++){
        for(let x=0; x <2; x++){
          for(let y=0; y < 2; y++){
        form.elements.firstNameInput().type(testData.valid.firstName[i])
        form.elements.lastNameInput().type(testData.valid.lastName[j])
        form.elements.emailInput().type(testData.valid.email[x])
        form.elements.phoneInput().type(testData.valid.phone[y])
        if(y==0){
          form.elements.genderInput("Male").check()
        }
        else if(y==1){
          form.elements.genderInput("Female").check()
        }
        form.elements.agreementInput().check()
        form.elements.submitBtn().click()
        cy.reload()
      }
    }
  }
}

  })
})