class formSubmit{
    elements = {
        firstNameInput : () => cy.get('#1'),
        firstNameValidation : () => cy.contains('Valid first name is required'),
        lastNameInput : () => cy.get('#2'),
        lasttNameValidation : () => cy.contains('Valid last name is required'),
        emailInput : () => cy.get('#3'),
        emailValidation : () => cy.contains('Valid email is required'),
        phoneInput : () => cy.get('#4'),
        phoneValidation : () => cy.contains('Valid phone number is required'),
        genderInput : (gender) => cy.get(`input[type="radio"][value=${gender}`),
        genderValidation : () => cy.contains('Choose your gender'),
        agreementInput : () => cy.get('#5'),
        agreementValidation : () => cy.contains('You must agree to the processing of personal data'),
        submitBtn : () => cy.get('#99'),
    }
}

module.exports = new formSubmit()