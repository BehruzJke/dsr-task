import { faker } from '@faker-js/faker';

class testData{
    valid = {
        firstName : [
            faker.random.alpha({ count: 2, casing: 'lower'}),
            faker.random.alpha({ count: 2, casing: 'upper'}),
            faker.random.alpha({ count: 25, casing: 'lower'}),
            faker.random.alpha({ count: 25, casing: 'upper'})
                    ],
        
            lastName : [
            faker.random.alpha({ count: 2, casing: 'lower'}),
            faker.random.alpha({ count: 2, casing: 'upper'}),
            faker.random.alpha({ count: 25, casing: 'lower'}),
            faker.random.alpha({ count: 25, casing: 'upper'})
                    ],

        email :     [
            faker.internet.email(),
            faker.internet.email('','','test.test'), 
                    ],
        
        phone :     [
            faker.phone.number('#######'),
            faker.phone.number('############')
                    ]
          }


    invalid = {
        firstName : [
            faker.random.alpha({ count: 1, casing: 'lower'}),
            faker.random.alpha({ count: 1, casing: 'upper'}),
            faker.random.alpha({ count: 26, casing: 'lower'}),
            faker.random.alpha({ count: 26, casing: 'upper'}),
            'a!','a@','a#','a$','a%','a^','a&','a*','a(','a)','a_','a+','\'--','\"--','<script>'
            ],

        lastName : [
            faker.random.alpha({ count: 1, casing: 'lower'}),
            faker.random.alpha({ count: 1, casing: 'upper'}),
            faker.random.alpha({ count: 26, casing: 'lower'}),
            faker.random.alpha({ count: 26, casing: 'upper'}),
            'a!','a@','a#','a$','a%','a^','a&','a*','a(','a)','a_','a+','\'--','\"--','<script>'
            ],

        email :   
        faker.random.alpha({ bannedChars: ['@','.'] }),

        phone :     [
            faker.random.numeric(6) ,
            faker.random.numeric(13) ,
            faker.random.alpha(7),
            faker.random.alphaNumeric(7)
            ]
    }
}

module.exports = new testData()