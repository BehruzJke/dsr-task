const { defineConfig } = require("cypress");

module.exports = defineConfig({ 
  e2e: {
    baseUrl:'https://vladimirwork.github.io/web-ui-playground/',
    setupNodeEvents(on, config) {
      // implement node event listeners here
    },
  },
  viewportWidth: 1000,
  viewportHeight: 600,
});
