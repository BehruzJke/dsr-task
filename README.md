### Test cases:

**TC-1**
Submit form contains all required elements

**TC-2**
User can't submit form with blank fields

**TC-3**
User can't submit form with invalid firstname

**TC-4**
User can't submit form with invalid lastname 

**TC-5**
User can't submit form with invalid email

**TC-6**
User can't submit form with invalid phone number

**TC-7**
User can't submit form without selecting gender

**TC-8**
User can't submit form without agreeing to process personal data

**TC-9**
User can submit form with all correct fields
